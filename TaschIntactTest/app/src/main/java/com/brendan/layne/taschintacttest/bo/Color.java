package com.brendan.layne.taschintacttest.bo;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;

import com.brendan.layne.taschintacttest.R;

public enum Color implements Parcelable {

    BLUE(R.color.blue_dark),
    BROWN(R.color.brown_leather),
    YELLOW(R.color.yellow),
    ORANGE(R.color.orange);

    private int colorId;

    Color(@ColorRes int colorId) {
        this.colorId = colorId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Color> CREATOR = new Creator<Color>() {
        @Override
        public Color createFromParcel(Parcel in) {
            return Color.values()[in.readInt()];
        }

        @Override
        public Color[] newArray(int size) {
            return new Color[size];
        }
    };

    public int getColorId() {
        return colorId;
    }
}
