package com.brendan.layne.taschintacttest.bo;

import android.os.Parcel;

public class Header implements ItemType {

    private int totalCost = 0;

    public Header() {}

    protected Header(Parcel in) {
        totalCost = in.readInt();
    }

    @Override
    public int getType() {
        return ItemType.TYPE_HEADER;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalCost);
    }

    public static final Creator<Header> CREATOR = new Creator<Header>() {
        @Override
        public Header createFromParcel(Parcel in) {
            return new Header(in);
        }

        @Override
        public Header[] newArray(int size) {
            return new Header[size];
        }
    };

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public int getTotalCost() {
        return totalCost;
    }
}
