package com.brendan.layne.taschintacttest.bo;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;

import com.brendan.layne.taschintacttest.R;

import java.util.ArrayList;
import java.util.List;

public class Product implements ItemType {

    private int id;
    @DrawableRes
    private int image;
    @StringRes
    private int title;
    private int price;
    @StringRes
    private int description;
    @StringRes
    private int longDescription;
    private boolean inStock;
    private List<Color> availableColors;
    @StringRes
    private int size;
    private boolean isInWishlist = false;

    public Product(int id, @DrawableRes int image, @StringRes int title, int price, @StringRes int description, @StringRes int longDescription, boolean inStock, List<Color> availableColors, @StringRes int size) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.price = price;
        this.description = description;
        this.longDescription = longDescription;
        this.inStock = inStock;
        this.availableColors = availableColors;
        this.size = size;
    }

    protected Product(Parcel in) {
        id = in.readInt();
        image = in.readInt();
        title = in.readInt();
        price = in.readInt();
        description = in.readInt();
        longDescription = in.readInt();
        inStock = in.readByte() != 0;
        availableColors = new ArrayList<>();
        in.readList(availableColors, Color.class.getClassLoader());
        size = in.readInt();
        isInWishlist = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(image);
        dest.writeInt(title);
        dest.writeInt(price);
        dest.writeInt(description);
        dest.writeInt(longDescription);
        dest.writeByte((byte) (inStock ? 1 : 0));
        dest.writeList(availableColors);
        dest.writeInt(size);
        dest.writeByte((byte) (isInWishlist ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public @DrawableRes
    int getImage() {
        return image;
    }

    public @StringRes
    int getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public @StringRes
    int getDescription() {
        return description;
    }

    public @StringRes
    int getLongDescription() {
        return longDescription;
    }

    public boolean isInStock() {
        return inStock;
    }

    @Override
    public int getType() {
        return ItemType.TYPE_PRODUCT;
    }

    public List<Color> getAvailableColors() {
        return availableColors;
    }

    public boolean isInWishlist() {
        return isInWishlist;
    }

    public @StringRes
    int getSize() {
        return size;
    }

    public void toggleIsInWishlist() {
        isInWishlist = !isInWishlist;
    }
}
