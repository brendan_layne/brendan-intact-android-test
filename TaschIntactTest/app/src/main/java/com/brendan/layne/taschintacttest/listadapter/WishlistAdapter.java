package com.brendan.layne.taschintacttest.listadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brendan.layne.taschintacttest.common.DataEvent;
import com.brendan.layne.taschintacttest.common.DataSource;
import com.brendan.layne.taschintacttest.R;
import com.brendan.layne.taschintacttest.bo.Header;
import com.brendan.layne.taschintacttest.bo.ItemType;
import com.brendan.layne.taschintacttest.bo.Product;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class WishlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final LayoutInflater inflater;
    private final DataSource<ItemType> dataSource;
    private final DataEvent dataEvent;

    public WishlistAdapter(Context context, DataSource<ItemType> products, DataEvent dataEvent) {
        this.inflater = LayoutInflater.from(context);
        this.dataSource = products;
        this.dataEvent = dataEvent;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ItemType.TYPE_HEADER) {
            return new WishlistHeaderRowHolder(inflater.inflate(R.layout.part_wishlist_header, parent, false));
        } else {
            return new WishlistCellRowHolder(inflater.inflate(R.layout.part_wishlist_cell, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == ItemType.TYPE_HEADER) {
            Header header = (Header) dataSource.getItem(position, true);
            WishlistHeaderRowHolder wishlistCellRowHolder = (WishlistHeaderRowHolder)holder;
            String totalCost = holder.itemView.getContext().getString(R.string.total) + " $" + header.getTotalCost();
            wishlistCellRowHolder.total.setText(totalCost);
        } else {
            Product product = (Product) dataSource.getItem(position, true);
            WishlistCellRowHolder wishlistCellRowHolder = (WishlistCellRowHolder)holder;
            wishlistCellRowHolder.image.setImageResource(product.getImage());
            String price = "$" + product.getPrice();
            wishlistCellRowHolder.price.setText(price);
            wishlistCellRowHolder.title.setText(product.getTitle());
            wishlistCellRowHolder.description.setText(product.getDescription());
            wishlistCellRowHolder.status.setVisibility(product.isInStock() ? GONE : VISIBLE);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return dataSource.getItemType(position, true);
    }

    @Override
    public int getItemCount() {
        return dataSource.getCount(true);
    }

    class WishlistHeaderRowHolder extends RecyclerView.ViewHolder {

        TextView total;

        WishlistHeaderRowHolder(View itemView) {
            super(itemView);
            total = itemView.findViewById(R.id.part_wishlist_header_total);
        }
    }

    class WishlistCellRowHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView price, title, description, status;

        WishlistCellRowHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.part_wishlist_cell_image);
            price = itemView.findViewById(R.id.part_wishlist_cell_price);
            title = itemView.findViewById(R.id.part_wishlist_cell_title);
            description = itemView.findViewById(R.id.part_wishlist_cell_description);
            status = itemView.findViewById(R.id.part_wishlist_cell_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataEvent.onClick(getAdapterPosition());
                }
            });
        }
    }
}
