package com.brendan.layne.taschintacttest.common;

public class Const {

    // Request codes
    public static final int REQUEST_CODE_OPEN_DETAIL = 100;

    // Bundle keys
    public static final String KEY_PRODUCT = "product";
    public static final String KEY_PRODUCT_ID = "product_id";
}
