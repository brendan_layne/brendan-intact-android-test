package com.brendan.layne.taschintacttest.presenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;

import com.brendan.layne.taschintacttest.bo.Product;
import com.brendan.layne.taschintacttest.common.Const;

public class DetailPresenter {

    public interface Viewable {
        void finish();

        void updateToolbarTitle(@StringRes int title);
        void showImage(@DrawableRes int imageResource);
        void showPrice(int price);
        void showLongDescription(@StringRes int description);
        void showAvailableColors();
        void showSize(@StringRes int size);
        void updateStatus(boolean isInWishlist);
        void toggleProductStatus(int productId);
    }

    private final Viewable viewable;

    private Product product;

    public DetailPresenter(Viewable viewable) {
        this.viewable = viewable;
    }

    public void onCreate(Intent intent, Bundle savedInstanceState) {
        final Bundle bundle;
        if(savedInstanceState == null) {
            bundle = intent.getExtras();
        } else {
            bundle = savedInstanceState;
        }

        if(bundle != null) {
            product = bundle.getParcelable(Const.KEY_PRODUCT);
            if(product != null) {
                viewable.updateToolbarTitle(product.getTitle());
                viewable.showImage(product.getImage());
                viewable.showPrice(product.getPrice());
                viewable.showLongDescription(product.getLongDescription());
                if(!product.getAvailableColors().isEmpty()) {
                    viewable.showAvailableColors();
                }
                viewable.showSize(product.getSize());
                viewable.updateStatus(product.isInWishlist());
            }
        } else {
            viewable.finish();
        }
    }

    public void onAddToWishClick() {
        int productId = product.getId();
        viewable.toggleProductStatus(productId);
    }

}
