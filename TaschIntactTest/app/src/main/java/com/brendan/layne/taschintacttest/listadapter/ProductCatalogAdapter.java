package com.brendan.layne.taschintacttest.listadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brendan.layne.taschintacttest.common.DataEvent;
import com.brendan.layne.taschintacttest.common.DataSource;
import com.brendan.layne.taschintacttest.R;
import com.brendan.layne.taschintacttest.bo.ItemType;
import com.brendan.layne.taschintacttest.bo.Product;

public class ProductCatalogAdapter extends RecyclerView.Adapter<ProductCatalogAdapter.ProductCatalogCellRowHolder> {

    private final LayoutInflater inflater;
    private final DataSource<ItemType> dataSource;
    private final DataEvent dataEvent;

    public ProductCatalogAdapter(Context context, DataSource<ItemType> products, DataEvent dataEvent) {
        this.inflater = LayoutInflater.from(context);
        this.dataSource = products;
        this.dataEvent = dataEvent;
    }

    @Override
    public ProductCatalogCellRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductCatalogCellRowHolder(inflater.inflate(R.layout.part_product_catalog_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductCatalogCellRowHolder holder, int position) {
        if(getItemViewType(position) == ItemType.TYPE_PRODUCT) {
            Product product = (Product) dataSource.getItem(position, false);

            holder.text.setText(product.getTitle());
            holder.image.setImageResource(product.getImage());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return dataSource.getItemType(position, false);
    }

    @Override
    public int getItemCount() {
        return dataSource.getCount(false);
    }

    class ProductCatalogCellRowHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView text;

        ProductCatalogCellRowHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.part_product_catalog_cell_image);
            text = itemView.findViewById(R.id.part_product_catalog_cell_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataEvent.onClick(getAdapterPosition() + 1);
                }
            });
        }
    }
}
