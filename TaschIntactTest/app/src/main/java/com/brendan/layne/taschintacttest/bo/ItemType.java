package com.brendan.layne.taschintacttest.bo;

import android.os.Parcelable;

public interface ItemType extends Parcelable {

    int TYPE_HEADER = 0;
    int TYPE_PRODUCT = 1;

    int getType();
}
