package com.brendan.layne.taschintacttest.common;

import android.app.Activity;
import android.content.Intent;

import com.brendan.layne.taschintacttest.activity.DetailActivity;
import com.brendan.layne.taschintacttest.bo.Product;

import static com.brendan.layne.taschintacttest.common.Const.KEY_PRODUCT;
import static com.brendan.layne.taschintacttest.common.Const.REQUEST_CODE_OPEN_DETAIL;

public class Navigation {

    public static void openDetail(Activity fromActivity, Product product) {
        Intent intent = new Intent(fromActivity, DetailActivity.class);
        intent.putExtra(KEY_PRODUCT, product);
        fromActivity.startActivityForResult(intent, REQUEST_CODE_OPEN_DETAIL);
    }
}
