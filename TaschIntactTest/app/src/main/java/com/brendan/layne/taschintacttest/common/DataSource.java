package com.brendan.layne.taschintacttest.common;

public interface DataSource<T> {

    int getCount(boolean isWishlist);

    T getItem(int position, boolean isWishlist);

    int getItemType(int position, boolean isWishlist);
}
