package com.brendan.layne.taschintacttest.common;

public interface DataEvent {
    void onClick(int position);
}
