package com.brendan.layne.taschintacttest.presenter;

import com.brendan.layne.taschintacttest.bo.Color;
import com.brendan.layne.taschintacttest.bo.Header;
import com.brendan.layne.taschintacttest.common.DataEvent;
import com.brendan.layne.taschintacttest.common.DataSource;
import com.brendan.layne.taschintacttest.R;
import com.brendan.layne.taschintacttest.bo.ItemType;
import com.brendan.layne.taschintacttest.bo.Product;

import java.util.ArrayList;
import java.util.List;

import static com.brendan.layne.taschintacttest.bo.Color.BLUE;
import static com.brendan.layne.taschintacttest.bo.Color.BROWN;
import static com.brendan.layne.taschintacttest.bo.Color.ORANGE;
import static com.brendan.layne.taschintacttest.bo.Color.YELLOW;

public class MainPresenter implements DataSource<ItemType>, DataEvent {


    private static final List<ItemType> PRODUCT_CATALOG = new ArrayList<ItemType>() {
        {
            add(new Product(0, R.drawable.carrier,
                    R.string.carrier_title,
                    250,
                    R.string.carrier_description,
                    R.string.carrier_long_description, true, new ArrayList<Color>() {
                {
                    add(BLUE);
                    add(BROWN);
                }
            }, R.string.carrier_size));

            add(new Product(1, R.drawable.urbanito,
                    R.string.urbanito_title,
                    75,
                    R.string.urbanito_description,
                    R.string.carrier_long_description,
                    false,
                    new ArrayList<Color>() {},
                    R.string.carrier_size));

            add(new Product(2, R.drawable.travol,
                    R.string.travol_title,
                    175,
                    R.string.travol_description,
                    R.string.carrier_long_description,
                    true,
                    new ArrayList<Color>() {
                {
                    add(BLUE);
                    add(YELLOW);
                    add(ORANGE);
                }
            }, R.string.carrier_size));
        }
    };

    public interface Viewable {
        void openDetail(Product product);

        void updateAdapters(int subtotal);
    }

    private final int INDEX_OF_HEADER = 0;

    private Viewable viewable;
    private final List<ItemType> products;

    public MainPresenter(Viewable viewable) {
        this.viewable = viewable;
        products = new ArrayList<>();
        products.addAll(PRODUCT_CATALOG);
        products.add(INDEX_OF_HEADER, new Header());
    }

    public void onToggleWishlist(int productId) {
        boolean updateAdapter = false;
        for(ItemType itemType : products) {
            if(itemType instanceof Product) {
                Product product = (Product)itemType;
                if(product.getId() == productId) {
                    product.toggleIsInWishlist();
                    updateAdapter = true;
                }
            }
        }

        int totalCost = 0;
        for(ItemType itemType : products) {
            if(itemType instanceof Product) {
                Product product = (Product)itemType;
                if(product.isInWishlist()) {
                    totalCost+= product.getPrice();
                }
            }
        }

        if(products.get(INDEX_OF_HEADER) instanceof Header) {
            Header header = (Header)products.get(INDEX_OF_HEADER);
            header.setTotalCost(totalCost);
        }

        if(updateAdapter) {
            viewable.updateAdapters(totalCost);
        }
    }

    @Override
    public void onClick(int position) {
        ItemType itemType = products.get(position);
        if(itemType.getType() == ItemType.TYPE_PRODUCT) {
            Product product = (Product)itemType;
            viewable.openDetail(product);
        }
    }

    @Override
    public int getCount(boolean isWishlist) {
        if(isWishlist) {
            int count = 0;
            for(ItemType itemType : products) {
                if(itemType instanceof Product) {
                    Product product = (Product) itemType;
                    if(product.isInWishlist()) {
                        count++;
                    }
                }
            }
            return count + 1;
        } else {
            return products.size() - 1;
        }
    }

    @Override
    public ItemType getItem(int position, boolean isWishlist) {
        if(isWishlist) {
            List<ItemType> wishlistProducts = new ArrayList<>();
            //make sure we get the correct item from the correct position
            for(ItemType itemType : products) {
                if(itemType instanceof Header) {
                    wishlistProducts.add(itemType);
                } else if(itemType instanceof Product) {
                    Product product = (Product)itemType;
                    if(product.isInWishlist()) {
                        wishlistProducts.add(itemType);
                    }
                }
            }

            return wishlistProducts.get(position);
        } else {
            return products.get(position + 1);
        }
    }

    @Override
    public int getItemType(int position, boolean isWishlist) {
        if(isWishlist) {
            return products.get(position).getType();
        } else {
            return products.get(position + 1).getType();
        }
    }
}
