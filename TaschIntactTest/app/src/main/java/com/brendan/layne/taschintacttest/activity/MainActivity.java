package com.brendan.layne.taschintacttest.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.brendan.layne.taschintacttest.R;
import com.brendan.layne.taschintacttest.bo.Product;
import com.brendan.layne.taschintacttest.common.Const;
import com.brendan.layne.taschintacttest.common.Navigation;
import com.brendan.layne.taschintacttest.listadapter.ProductCatalogAdapter;
import com.brendan.layne.taschintacttest.listadapter.WishlistAdapter;
import com.brendan.layne.taschintacttest.presenter.MainPresenter;
import com.brendan.layne.taschintacttest.presenter.MainPresenter.Viewable;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.graphics.Typeface.BOLD;
import static com.brendan.layne.taschintacttest.common.Const.REQUEST_CODE_OPEN_DETAIL;

public class MainActivity extends AppCompatActivity implements Viewable {

    @BindView(R.id.activity_main_product_catalog)
    RecyclerView recyclerViewProductCatalog;
    @BindView(R.id.activity_main_product_wishlist)
    RecyclerView recyclerViewWishlist;
    @BindView(R.id.activity_main_subtotal)
    TextView subtotal;
    @BindView(R.id.activity_main_checkout_button)
    Button checkoutButton;

    private ProductCatalogAdapter adapterProductCatalog;
    private WishlistAdapter adapterWishlist;

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter(this);

        adapterProductCatalog = new ProductCatalogAdapter(this, presenter, presenter);
        recyclerViewProductCatalog.setAdapter(adapterProductCatalog);
        adapterWishlist = new WishlistAdapter(this, presenter, presenter);
        recyclerViewWishlist.setAdapter(adapterWishlist);

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.Dialog);
                builder.setMessage(MainActivity.this.getString(R.string.dialog_message));
                builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_OPEN_DETAIL && resultCode == RESULT_OK) {
            int productId = data.getIntExtra(Const.KEY_PRODUCT_ID, -1);
            if(productId != -1) {
                presenter.onToggleWishlist(productId);
            }

        }
    }

    @Override
    public void openDetail(Product product) {
        Navigation.openDetail(this, product);
    }

    @Override
    public void updateAdapters(int subtotal) {
        adapterWishlist.notifyDataSetChanged();
        adapterProductCatalog.notifyDataSetChanged();

        String subtotalValue = getString(R.string.subtotal) + " $" + subtotal;
        SpannableString coloredSubtotal = new SpannableString(subtotalValue);
        coloredSubtotal.setSpan(new StyleSpan(BOLD), 0, getString(R.string.subtotal).length(), 0);
        this.subtotal.setText(coloredSubtotal);
    }
}