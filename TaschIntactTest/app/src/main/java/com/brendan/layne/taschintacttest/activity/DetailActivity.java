package com.brendan.layne.taschintacttest.activity;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.brendan.layne.taschintacttest.R;
import com.brendan.layne.taschintacttest.common.Const;
import com.brendan.layne.taschintacttest.presenter.DetailPresenter;
import com.brendan.layne.taschintacttest.presenter.DetailPresenter.Viewable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements Viewable {

    private DetailPresenter presenter;

    @BindView(R.id.activity_detail_image)
    ImageView image;
    @BindView(R.id.activity_detail_price)
    TextView price;
    @BindView(R.id.activity_detail_description)
    TextView description;
    @BindView(R.id.activity_detail_size)
    TextView size;
    @BindView(R.id.activity_detail_add_to_wishlist)
    ToggleButton addToWishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        presenter = new DetailPresenter(this);
        presenter.onCreate(getIntent(), savedInstanceState);

        addToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAddToWishClick();
            }
        });
    }

    @Override
    public void updateToolbarTitle(@StringRes int title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showImage(@DrawableRes int imageResource) {
        image.setImageResource(imageResource);
    }

    @Override
    public void showPrice(int price) {
        String priceValue = "$ " + String.valueOf(price);
        this.price.setText(priceValue);
    }

    @Override
    public void showLongDescription(@StringRes int longDescription) {
        description.setText(longDescription);
    }

    @Override
    public void showAvailableColors() {
        // TODO: 2/27/2018
    }

    @Override
    public void showSize(@StringRes int size) {
        this.size.setText(size);
    }

    @Override
    public void updateStatus(boolean isInWishlist) {
        addToWishList.setChecked(!isInWishlist);
    }

    @Override
    public void toggleProductStatus(int productId) {
        getIntent().putExtra(Const.KEY_PRODUCT_ID, productId);
        setResult(RESULT_OK, getIntent());
        finish();
    }
}
